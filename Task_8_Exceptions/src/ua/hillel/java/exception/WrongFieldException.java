package ua.hillel.java.exception;

import java.io.IOException;

public class WrongFieldException {
    public void throwCheckedException() throws IOException{
        throw new IOException("WrongFieldException");
    }
}
