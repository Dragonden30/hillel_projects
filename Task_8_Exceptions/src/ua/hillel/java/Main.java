package ua.hillel.java;

import ua.hillel.java.service.TransactionService;
import ua.hillel.java.util.UtilityHelper;

import java.io.IOException;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        TransactionService transactionService = new TransactionService();
        UtilityHelper utilityHelper = new UtilityHelper();
        String acc_string;
        String acc2_string;
        String sum_string;
        Scanner scanner = new Scanner(System.in);

        while (true) {
            try {
                System.out.println("Enter your account id:");
                acc_string = scanner.nextLine();
                utilityHelper.accountIdentifierHelper(acc_string);
                System.out.println("You entered account 1: " + acc_string);
                break;
            } catch (IOException e) {
                System.out.println("Please enter only 10 digits");
            }
        }

        while (true) {
            try {
                System.out.println("Enter your partner account id:");
                acc2_string = scanner.nextLine();
                utilityHelper.accountIdentifierHelper(acc2_string);
                System.out.println("You entered account 2: " + acc2_string);
                break;
            } catch (IOException e) {
                System.out.println("Please enter only 10 digits");
            }
        }

        while (true) {
            try {
                System.out.println("Enter your transaction sum:");
                sum_string = scanner.nextLine();
                utilityHelper.sumHelper(sum_string);
                System.out.println("You entered sum: " + sum_string);
                break;
            } catch (IOException e) {
                System.out.println("Please less or equal then 1000");
            }
        }

        transactionService.moneyTransaction(acc_string, acc2_string);

    }
}
