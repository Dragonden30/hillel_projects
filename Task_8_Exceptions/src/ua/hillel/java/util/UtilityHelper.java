package ua.hillel.java.util;

import ua.hillel.java.exception.UserExpectedError;
import ua.hillel.java.exception.WrongFieldException;
import ua.hillel.java.exception.WrongSumException;

import java.io.IOException;

public class UtilityHelper {

    public void accountIdentifierHelper(String clientaccountid) throws IOException {
        String pattern = "^\\d{10}$";
        WrongFieldException wrongFieldException = new WrongFieldException();
        if (clientaccountid.matches(pattern) == false){
            wrongFieldException.throwCheckedException();
        }
    }

    public void sumHelper(String sum) throws IOException {
        WrongSumException wrongSumException = new WrongSumException();
        Integer number = Integer.valueOf(sum);
        if (number > 1000){
            wrongSumException.throwCheckedException();
        }
    }

    public void accComparisonHelper(String initialclientaccountid, String receiverclientaccountid){
        UserExpectedError userExpectedError = new UserExpectedError();
        if (initialclientaccountid.equals(receiverclientaccountid)){
            userExpectedError.throwUncheckedException();
        }

    }
}
