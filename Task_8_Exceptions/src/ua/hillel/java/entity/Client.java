package ua.hillel.java.entity;

public class Client {

    private static int client_id; //initial account id
    private static String last_name; //name on initial account id
    private static String client_acc_id; //receiver id
    private static double sum; //money

    public Client(int client_id, String last_name, String client_acc_id, double sum) {
        Client.client_id = client_id;
        Client.last_name = last_name;
        Client.client_acc_id = client_acc_id;
        Client.sum = sum;
    }

    public static int getClient_id() {
        return client_id;
    }

    public static void setClient_id(int client_id) {
        Client.client_id = client_id;
    }

    public static String getLast_name() {
        return last_name;
    }

    public static void setLast_name(String last_name) {
        Client.last_name = last_name;
    }

    public static String getClient_acc_id() {
        return client_acc_id;
    }

    public static void setClient_acc_id(String client_acc_id) {
        Client.client_acc_id = client_acc_id;
    }

    public static double getSum() {
        return sum;
    }

    public static void setSum(double sum) {
        Client.sum = sum;
    }
}
