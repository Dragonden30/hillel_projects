package org.example;

import org.example.entity.User;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        final User user = new User();
        user.setName("Denys");
        user.setSurname("Maksymovych");
        user.setAge(23);

        System.out.println(user.getName() + user.getSurname() + user.getAge());
    }
}