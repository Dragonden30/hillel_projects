package ua.hillel.java;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.*;

public class Main {
    public static void main(String[] args) {

        final Predicate<Double> predicate = (inputDouble) -> inputDouble > 1500.0;
        System.out.println("Predicate: Inputed 1250.0 = " + predicate.test(1250.0));
        System.out.println("Predicate: Inputed 1501.0 = " + predicate.test(1501.0));

        final Consumer<String> consumer = (inputString) -> System.out.println("Consumer: " +
                Arrays.toString(inputString.toCharArray()));
        consumer.accept("rent");

        final String[] onedigit = {" One", " Two", " Three", " Four", " Five",
                " Six", " Seven", " Eight", " Nine", " Ten"};

        final Function<Integer, String> function = (functionIntager) -> {
           if ((1 <= functionIntager)&& (functionIntager <= 10)) return onedigit[functionIntager - 1];
               else return "unknown";
        };

        System.out.println("Function inputed 1: " + function.apply(1));
        System.out.println("Function inputed 11: " + function.apply(11));

        final Supplier <Integer> supplier = () -> 10;

        System.out.println("Supplier: " + supplier.get());

        final BiFunction<Double, Integer, String> biFunction = (biDouble, biIntager) -> {
            if((biDouble-biIntager) < 1) return "Result less then 1";
            else return "Result more then 1";
        };

        System.out.println("BiFunction inputed 10.1 and 10: " + biFunction.apply(10.1, 10));
        System.out.println("BiFunction inputed 11.1 and 10: " + biFunction.apply(11.1, 10));

        final FiveDigitFunction<Double, Integer, Boolean, Integer, String> fiveDigitFunction =
        (fiveDouble, fiveInteger, fiveBoolean, fiveInteger2) -> {
            if(fiveInteger2 >= 1){
                if(fiveBoolean){
                        return "This mutant equals to: " + ((fiveDouble / fiveInteger2) + (fiveInteger / fiveInteger2));
                } else {
                        return "This mutant equals to: " + ((fiveDouble * fiveInteger2) - (fiveInteger * fiveInteger2));
                }

            } else return "To low number";
        };
        System.out.println("FiveDigitFunction inputed 12.5, 10, true, 2: " +
                fiveDigitFunction.apply(12.5, 10, true, 2));
        System.out.println("FiveDigitFunction inputed 12.5, 10, true, 2: " +
                fiveDigitFunction.apply(12.5, 10, false, 3));

        final Map<String, Integer> map = new HashMap<>();
        map.put("Denis", 23);
        map.put("Timur", 12);

        map.forEach((name, age) -> System.out.println(name + " is " + age + " years old"));
    }
}