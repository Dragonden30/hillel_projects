package ua.hillel.java;

@FunctionalInterface
public interface FiveDigitFunction<T, U, R, K, E> {
    E apply(T t, U u, R r, K k);
}
