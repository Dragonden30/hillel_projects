package ua.hillel.java;

import java.util.Arrays;

public class Structure {

    //structure

    byte bytevar = 10;
    short shortvar = 110;
    long longvar = 1000000L;
    float floatvar = 1432.21f;
    char charvar = 95;
    boolean boolvar = true;
    int intvar1 = 10, intvar2 = 20, intvar3 = 30;
    double doublevar1 = 10.123, doublevar2 = 20.123, doublevar3 = 30.123;

    public void mathematics(){
        System.out.println("Mathematics (Task 3)");
        int ex1 = (shortvar + intvar1) / intvar2;
        int ex2 = shortvar * intvar1 - intvar2;
        int ex3 = intvar3 + intvar2 /intvar1;
        int ex4 = intvar2 + (shortvar - intvar3);
        int ex5 = shortvar * (intvar1 * intvar2);
        double ex6 = doublevar1 * doublevar2 - doublevar3;
        double ex7 = doublevar1 / intvar1 * doublevar2;
        double ex8 = doublevar3 - (intvar2 - intvar1);
        double ex9 = doublevar2 * intvar1 - doublevar1;
        double ex10 = intvar1 / intvar2 - doublevar1;
        System.out.println(" Example 1: " + ex1 + "\n Example 2: " + ex2 + "\n Example 3: " + ex3 +
                "\n Example 4: " + ex4 + "\n Example 5: " + ex5 + "\n Example 6: " + ex6 + "\n Example 7: " + ex7
                + "\n Example 8: " + ex8 + "\n Example 9: " + ex9 + "\n Example 10: " + ex10);
    }

    public void mathematicsAssignment(){
        System.out.println("Mathematics with Assignment (Task 4)");
        int ex1 = shortvar += intvar1;
        int ex2 = shortvar -= intvar1;
        int ex3 = intvar3 *= intvar1;
        int ex4 = shortvar /= intvar2;
        int ex5 = shortvar %= intvar2;
        double ex6 = doublevar1 += intvar1;
        double ex7 = doublevar1 -= doublevar2;
        double ex8 = doublevar3 += intvar2;
        double ex9 = doublevar2 /= intvar2;
        double ex10 = intvar1 %= intvar2;
        System.out.println(" Example 1: " + ex1 + "\n Example 2: " + ex2 + "\n Example 3: " + ex3 +
                "\n Example 4: " + ex4 + "\n Example 5: " + ex5 + "\n Example 6: " + ex6 + "\n Example 7: " + ex7
                + "\n Example 8: " + ex8 + "\n Example 9: " + ex9 + "\n Example 10: " + ex10);
    }

    public void logical(){
        System.out.println("Logical (Task 5)");
        boolean ex1 = shortvar > intvar1;
        boolean ex2 = shortvar >= intvar1;
        boolean ex3 = intvar3 < intvar1;
        boolean ex4 = intvar2 == intvar2;
        boolean ex5 = shortvar <= intvar2;
        System.out.println(" Example 1: " + ex1 + "\n Example 2: " + ex2 + "\n Example 3: " + ex3 +
                "\n Example 4: " + ex4 + "\n Example 5: " + ex5);
    }

    public void incrementDecrement(){
        System.out.println("Increment and decrement (Task 6)");
        int var = 1;
        System.out.println(" " + var + "\n " + var-- + "\n " + var++ +
                "\n " + var + "\n " + var + "\n " + var + "\n " + var++ +
                "\n " + var-- + "\n " + var);
    }

    public void array(){
        System.out.println("Array (Task 7)");
        int[] arr = new int[10];
        arr[0] = 2;
        arr[1] = 3;
        arr[2] = 13;
        arr[3] = 76;
        arr[4] = 54;
        arr[5] = 26;
        arr[6] = 37;
        arr[7] = 35;
        arr[8] = 12;
        arr[9] = 34;
        System.out.println(" Array : " + Arrays.toString(arr));
    }

}
