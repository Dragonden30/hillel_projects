package ua.hillel.java;

public class Main {

    public static void main(String[] args) {
        Structure structure = new Structure();
        structure.mathematics();
        structure.mathematicsAssignment();
        structure.logical();
        structure.incrementDecrement();
        structure.array();
    }
}
