package ua.hillel.java;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) {
     final int THREAD_POOL_SIZE = 2;
     final int TASK_COUNT = 10000;
     final int THREAD_COUNT = THREAD_POOL_SIZE * TASK_COUNT;

     ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

     AtomicInteger counter = new AtomicInteger(THREAD_COUNT);

         for (int i = 0; i < THREAD_POOL_SIZE; i++) {
             executorService.execute(() -> {
                 for (int j = 0; j < TASK_COUNT; j++) {
                     Thread task1thread = new Thread(() -> {
                         //System.out.println(counter.get());
                         counter.decrementAndGet();
                     });
                     task1thread.start();
                 }
             });
        }

        executorService.shutdown();
        while (!executorService.isTerminated()) {}

        System.out.println("Result: " + counter.get());

        LockMechods lockMechods = new LockMechods();
        Thread task2thread1 = new Thread(() -> {
            System.out.println("Thread 1 \n");
            lockMechods.method1(22,3,2023);
            lockMechods.method2(12,6,50);
            lockMechods.method3(123,2);
        });
        Thread task2thread2 = new Thread(() -> {
            System.out.println("Thread 2 \n");
            lockMechods.method1(12, 3, 2023);
            lockMechods.method2(14, 45, 23);
            lockMechods.method3(140, 3);
        });
        Thread task2thread3 = new Thread(() -> {
            System.out.println("Thread 3 \n");
            lockMechods.method1(6,2,2023);
            lockMechods.method2(6,25,12);
            lockMechods.method3(250,4);
        });
        task2thread1.start();
        task2thread2.start();
        task2thread3.start();
        try {
            task2thread1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            task2thread2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            task2thread3.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        new DeadlockExample();

    }

}
