package ua.hillel.java;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockMechods {
    private final Lock lock = new ReentrantLock();

    public void method1(int day, int month, int year) {
        lock.lock();
        try {
            System.out.println("First Method \n");
            System.out.println("Sum of date numbers: " + (day + month + year) + " \n");
        } finally {
            lock.unlock();
        }
    }

    public void method2(int hour, int minute, int seconds) {
        lock.lock();
        try {
            System.out.println("Second Method \n");
            System.out.println("Seconds you wasted today: " + (hour*360 + minute*60 + seconds) + " \n");
        } finally {
            lock.unlock();
        }
    }

    public void method3(int calories, int meals) {
        lock.lock();
        try {
            System.out.println("Third Method \n");
            System.out.println("Calories could gain: " + (calories * meals) + " \n");
        } finally {
            lock.unlock();
        }
    }
}
