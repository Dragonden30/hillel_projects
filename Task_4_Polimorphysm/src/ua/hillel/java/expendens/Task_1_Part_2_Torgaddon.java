package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_2_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_2_Rules;

public final class Task_1_Part_2_Torgaddon extends Task_1_Part_2_MournivalMembers
        implements Task_2_Part_2_Rules, Task_2_Part_2_AdvancedRules {

    @Override
    public void duty(){
        System.out.println("Tarik Torgaddon was the Captain of the Luna Wolves 2nd Company " +
                "and a member of the elite Mournival, an informal council of four captains meant " +
                "to advise the Legion's primarch, Horus. He was known as a joker but an extremely skilled warrior.");
    }

    @Override
    public void train(){
        System.out.println("Torgaddon is training extremely well.");
    }

    @Override
    public void pray(){
        System.out.println("Torgaddon is praying for Warmasters luck.");
    }

    @Override
    public void read(){
        System.out.println("Torgaddon reads about war tactics.");
    }

}
