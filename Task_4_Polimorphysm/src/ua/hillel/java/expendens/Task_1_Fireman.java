package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_1_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_1_Rules;

public final class Task_1_Fireman extends Task_1_Highrisk implements Task_2_Part_1_AdvancedRules,
        Task_2_Part_1_Rules {
    @Override
    public void work(){
        System.out.println("All professions are great! But here men are Men.");
    }

    @Override
    public void relax(){
        System.out.println("We relaxing. But a bit.");
    }

    @Override
    public void hear(){
        System.out.println("We hear everything even when relaxing.");
    }

    @Override
    public void scream(){
        System.out.println("We scream but alarm scream louder.");
    }
}
