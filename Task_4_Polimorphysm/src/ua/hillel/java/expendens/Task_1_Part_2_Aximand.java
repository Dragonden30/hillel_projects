package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_2_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_2_Rules;

public final class Task_1_Part_2_Aximand extends Task_1_Part_2_MournivalMembers
        implements Task_2_Part_2_Rules, Task_2_Part_2_AdvancedRules {

    @Override
    public void duty(){
        System.out.println("Horus Aximand was a Captain of the Luna Wolves Space Marine Legion," +
                " and one of four members of the elite Mournival council. His likeness to Warmaster Horus was" +
                " so striking that his peers affectionately referred to him as \"Little Horus.\"Despite Aximand's" +
                " allegiances to the Warmaster, he is always described as a wise and rational Astartes." +
                " He had subordinated his personality to allow him to follow Horus's banner into betrayal" +
                " and civil war.");
    }

    @Override
    public void train(){
        System.out.println("Aximand is training like Warmaster.");
    }

    @Override
    public void pray(){
        System.out.println("Aximand don't pray, religion is heresy.");
    }

    @Override
    public void read(){
        System.out.println("Aximand reads a lot.");
    }

}
