package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_2_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_2_Rules;

public final class Task_1_Part_2_Loken extends Task_1_Part_2_MournivalMembers
        implements Task_2_Part_2_Rules, Task_2_Part_2_AdvancedRules {

    @Override
    public void duty(){
        System.out.println("Garviel Loken was the Captain of the 10th company of the Luna Wolves Space Marine Legion " +
                "during the latter half of the Great Crusade. After distinguishing himself in battle, " +
                "he was inducted into the Mournival, the advisory council to the Warmaster Horus, " +
                "and from this position was a first-hand witness to the series of events that would result in Horus' " +
                "damnation and the beginning of the Horus Heresy.");
    }

    @Override
    public void train(){
        System.out.println("Loken is training well for a newbie.");
    }

    @Override
    public void pray(){
        System.out.println("Loken don't pray, religion is heresy.");
    }

    @Override
    public void read(){
        System.out.println("Loken reads data logs.");
    }

}
