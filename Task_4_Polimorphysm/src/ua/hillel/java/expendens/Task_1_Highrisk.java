package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_1_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_1_Rules;

public abstract class Task_1_Highrisk extends Task_1_Professions implements Task_2_Part_1_AdvancedRules,
        Task_2_Part_1_Rules {

    @Override
    public void work(){
        System.out.println("All professions are great! But these are harder.");
    }

    @Override
    public void relax(){
        System.out.println("Everyone relaxing. But harder.");
    }

    public void hear(){
        System.out.println("We hear everything but harder.");
    }

    public void scream(){
        System.out.println("We scream but harder.");
    }

}
