package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_1_Rules;

public abstract class Task_1_Professions implements Task_2_Part_1_Rules {

    public void work(){
        System.out.println("All professions are great!");
    }

    public void relax(){
        System.out.println("Everyone relaxing.");
    }

}
