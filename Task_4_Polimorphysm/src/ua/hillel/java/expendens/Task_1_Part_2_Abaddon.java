package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_2_AdditionalRules;
import ua.hillel.java.implementends.Task_2_Part_2_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_2_Rules;

public final class Task_1_Part_2_Abaddon extends Task_1_Part_2_MournivalMembers
        implements Task_2_Part_2_Rules, Task_2_Part_2_AdvancedRules, Task_2_Part_2_AdditionalRules {

    @Override
    public void duty(){
        System.out.println("Ezekyle Abaddon, more commonly known as Abaddon the Despoiler, is the Warmaster of Chaos," +
                " the former First Captain of the Sons of Horus Legion and now absolute Master of the Black Legion," +
                " and rumoured to be the clone-progeny of Warmaster Horus. He is the most powerful Warmaster of all," +
                " successor to Horus, and blessed by all four of the Gods of Chaos. " +
                "Despite being the Warmaster of Chaos, Abaddon has refused giving himself fully over" +
                " to the Ruinous Powers as the Daemon Primarchs have, as this would limit his existence" +
                " beyond the Eye of Terror and push his ultimate vengeance against the Imperium beyond his grasp.");
    }

    @Override
    public void train(){
        System.out.println("Abaddon is training like Warmaster.");
    }

    @Override
    public void pray(){
        System.out.println("Abaddon don't pray, religion is heresy.");
    }

    @Override
    public void read(){
        System.out.println("Abaddon reads a lot.");
    }

    public void betray(){
        System.out.println("Abaddon betrays his brothers.");
    }

    public void kill(){
        System.out.println("Abaddon kills Loken.");
    }

}
