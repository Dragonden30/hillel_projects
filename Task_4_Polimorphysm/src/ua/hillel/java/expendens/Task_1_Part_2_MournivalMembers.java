package ua.hillel.java.expendens;

import ua.hillel.java.implementends.Task_2_Part_2_AdvancedRules;
import ua.hillel.java.implementends.Task_2_Part_2_Rules;

public abstract class Task_1_Part_2_MournivalMembers extends Task_1_Part_2_LunaWolvesHierarchy
        implements Task_2_Part_2_Rules, Task_2_Part_2_AdvancedRules {

    @Override
    public void duty(){
        System.out.println("For the Horus we stand.");
    }

    @Override
    public void train(){
        System.out.println("Even commanders of Gods have to train.");
    }

    public void pray(){
        System.out.println("Commanders are praying.");
    }

    public void read(){
        System.out.println("Commanders are reading.");
    }

}
