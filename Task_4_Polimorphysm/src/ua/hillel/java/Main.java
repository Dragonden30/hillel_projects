package ua.hillel.java;

import ua.hillel.java.expendens.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("\n---------Part 1---------\n");
        final Task_1_Policeman policeman = new Task_1_Policeman();
        final Task_1_Fireman fireman = new Task_1_Fireman();
        final Task_1_Schoolteacher schoolteacher = new Task_1_Schoolteacher();

        System.out.println("\nPoliceman\n");
        policeman.work();
        policeman.relax();
        policeman.hear();
        policeman.scream();
        System.out.println("\nFireman\n");
        fireman.work();
        fireman.relax();
        fireman.hear();
        fireman.scream();
        System.out.println("\nSchoolteacher\n");
        schoolteacher.work();
        schoolteacher.relax();
        schoolteacher.hear();
        schoolteacher.scream();

        //final Task_1_Highrisk highrisk = new Task_1_Highrisk();  Can't be implenemted
        //final Task_1_Professions professions = new Task_1_Professions();  Can't be implenemted

        System.out.println("\n---------Part 2---------\n");
        final Task_1_Part_2_Abaddon abaddon = new Task_1_Part_2_Abaddon();
        final Task_1_Part_2_Torgaddon torgaddon = new Task_1_Part_2_Torgaddon();
        final Task_1_Part_2_Aximand aximand = new Task_1_Part_2_Aximand();
        final Task_1_Part_2_Loken loken = new Task_1_Part_2_Loken();

        System.out.println("\nAbaddon\n");
        abaddon.duty();
        abaddon.train();
        abaddon.pray();
        abaddon.read();
        abaddon.betray();
        abaddon.kill();
        System.out.println("\nTorgaddon\n");
        torgaddon.duty();
        torgaddon.train();
        torgaddon.pray();
        torgaddon.read();
        System.out.println("\nAximand\n");
        aximand.duty();
        aximand.train();
        aximand.pray();
        aximand.read();
        System.out.println("\nLoken\n");
        loken.duty();
        loken.train();
        loken.pray();
        loken.read();

        //final Task_1_Part_2_LunaWolvesHierarchy hierarchy = new Task_1_Part_2_LunaWolvesHierarchy();  Can't be implenemted
        //final Task_1_Part_2_MournivalMembers mournivalMembers = new Task_1_Part_2_MournivalMembers();  Can't be implenemted

    }
}
