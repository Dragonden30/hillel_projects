package ua.hillel.java.implementends;

public interface Task_2_Part_2_AdvancedRules extends Task_2_Part_2_Rules {

    @Override
    public void duty();

    @Override
    public void train();

    public void pray();

    public void read();

}
