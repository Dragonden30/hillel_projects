package ua.hillel.java;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        final List<String> empty =  Stream.empty()
                                            .skip(1)
                                            .map(var -> var + "_checked")
                                            .collect(Collectors.toList());

        System.out.println("First task - Empty Set: " + empty);

        final Set<String> filedSet =  Stream.of("Day3",  "Day2", "Day1")
                                            .map(value -> value + "_element_of_Set")
                                            .limit(2)
                                            .collect(Collectors.toSet());

        System.out.println("Second task - List to Set: " + filedSet);

        Set<String> setForTaskThree = Stream.of("a", "b", "c")
                .collect(Collectors.toCollection(HashSet::new));

        final List<String> listOutOfSet =  setForTaskThree.stream()
                .collect(Collectors.toList());

        System.out.println("Third task - List out of Set: " + listOutOfSet +
                "\n             list check: " + listOutOfSet.get(1));

        System.out.println("Fourth task - Hashmap stream: ");

        HashMap<Integer, String> hashMapToTaskFour = new HashMap<>();

        hashMapToTaskFour.put(100, "Hundred");
        hashMapToTaskFour.put(101, "HundredOne");
        hashMapToTaskFour.put(102, "HundredTwo");
        System.out.println("Verifying hashMap: " + hashMapToTaskFour + " key 100 = " + hashMapToTaskFour.get(100));

        hashMapToTaskFour.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(System.out::println);

        System.out.println("\nFifth task - List mapping: ");

        List<String> listForMapping = new ArrayList<String>();

        listForMapping.add(" 123");
        listForMapping.add(" 345 ");
        listForMapping.add("567 ");
        listForMapping.add(" 789 ");
        listForMapping.add("902  ");
        listForMapping.add(" 299 ");
        listForMapping.add(" 419");
        listForMapping.add("676 ");
        listForMapping.add("868 ");
        listForMapping.add(" 238");

        final List<Integer> listOfMappedNumbers = listForMapping.stream()
                .map(String::trim)
                .skip(0)
                .limit(8)
                .mapToInt(Integer::parseInt)
                .filter(value -> value % 2 == 0)
                .distinct()
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        System.out.println(listOfMappedNumbers);

        System.out.println("\nSixth task - Parallel Stream: ");

        final Optional<String> parallelStream = Stream.of("Kiev", "Odessa", "Lviv", "Donetsc")
                .parallel()
                .sorted()
                .filter(value -> value.length() <= 4)
                .findFirst();

        System.out.println(parallelStream);

        System.out.println("\nSeventh task - Reference in Stream: ");

        final List<String> References = listForMapping.stream()
                .map(String::trim)
                .map(Integer::parseInt)
                .map(Numbers::new)
                .map(Numbers::toString)
                .toList();

        System.out.println(References);
    }


}
