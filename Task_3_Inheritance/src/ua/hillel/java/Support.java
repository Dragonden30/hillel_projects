package ua.hillel.java;

public class Support extends Users {

    public final StringBuilder TEST = new StringBuilder("Helping is good," + getNewLine() +
            "Killing is bad," + getNewLine() + "Some drugs are good," +
            getNewLine() + "Some are bad.");

    public Support(String name, String surname, String email, int pass, String sex, String country) {
        super(name, surname, email, pass, sex, country);
    }

    public boolean check(String subject) {
        System.out.println("Contains? : " + subject.contains(TEST));
        return subject.contains(TEST);
    }
}
