package ua.hillel.java;

public class Main {
    private static void process(Users user) {
        System.out.println(user.getTextHolder());
        if (user instanceof Support) {
            boolean contains = ((Support) user).check(user.getTextHolder());
            if (user instanceof Admin) {
                ((Admin) user).checkReplace(contains);
            }
        }
    }

    public static void main(String[] args) {
        Users testUser = new Users("Denis", "Maksymovych", "denking30@gmail.com", 10062022,
                "male", "Ukraine");
        Support testSupport = new Support("Denis", "Maksymovych", "denking30@gmail.com", 10062022,
                "male", "Ukraine");
        Admin testAdmin = new Admin("Denis", "Maksymovych", "denking30@gmail.com", 10062022,
                "male", "Ukraine");
        testUser.read("This is the test writing.");
        testUser.write();
        testSupport.setTextHolder(testUser.getTextHolder());
        testAdmin.setTextHolder(testUser.getTextHolder());
        System.out.println("\nUser: ");
        process(testUser);
        System.out.println("\nSupport: ");
        process(testSupport);
        System.out.println("\nAdmin: ");
        process(testAdmin);
    }
}
