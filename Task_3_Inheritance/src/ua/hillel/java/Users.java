package ua.hillel.java;

import java.util.Scanner;

public class Users {
    private String name;
    private String surname;
    private String email;
    private int pass;
    private String sex;
    private String country;
    private boolean switchBool;
    private String textHolder;
    private final String newLine = System.getProperty("line.separator");
    public Users(String name, String surname, String email, int pass, String sex, String country) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.pass = pass;
        this.sex = sex;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPass() {
        return pass;
    }

    public void setPass(int pass) {
        this.pass = pass;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isSwitchBool() {
        return switchBool;
    }

    public void setSwitchBool(boolean switchBool) {
        this.switchBool = switchBool;
    }

    public String getTextHolder() {
        return textHolder;
    }

    public void setTextHolder(String textHolder) {
        this.textHolder = textHolder;
    }

    public String getNewLine() {
        return newLine;
    }

    public void read(String text){
        System.out.println(text);
    }

    public void write(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a quatrains that in Support class " +
                "(after every line push enter to skip, max is 3 line skips): ");

        StringBuilder text = new StringBuilder(scan.nextLine() + newLine +
                scan.nextLine() + newLine + scan.nextLine() +
                newLine + scan.nextLine());

        System.out.println("You wrote this: \n" + text);

        setTextHolder(String.valueOf(text));
    }
}
