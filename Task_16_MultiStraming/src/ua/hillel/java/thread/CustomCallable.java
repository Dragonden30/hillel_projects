package ua.hillel.java.thread;

import java.util.concurrent.Callable;

public class CustomCallable implements Callable<Integer> {

    @Override
    public Integer call() {
            int result = CyclickCounter.processThread(Thread.currentThread().getName());
            return result;
    }
}

