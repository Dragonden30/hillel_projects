package ua.hillel.java.thread;

public class CustomRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("CustomRunnable is running....");
        for (int i = 0; i < 1000; i++) {

            CyclickCounter.processThread(Thread.currentThread().getName());

        }
    }
}

