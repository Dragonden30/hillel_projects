package ua.hillel.java.thread;

public class CustomThread extends Thread {

    @Override
    public void run() {
        System.out.println("CustomThread is running....");
        for (int i = 0; i < 1000; i++) {

            CyclickCounter.processThread(Thread.currentThread().getName());

        }
    }
}

