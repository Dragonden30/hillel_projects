package ua.hillel.java.thread;

public class CyclickCounter {
    private static int counter = 0;

    public static int processThread(String threadName) {
        System.out.println("Thread name: " + threadName + ", counter value: " + counter);
        counter++;
        return counter;
    }
}
