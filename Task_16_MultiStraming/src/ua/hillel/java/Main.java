package ua.hillel.java;

import ua.hillel.java.thread.CustomCallable;
import ua.hillel.java.thread.CustomRunnable;
import ua.hillel.java.thread.CustomThread;

public class Main {

    public static void main(String[] args) {

         final CustomThread customThread = new CustomThread();
         customThread.run();
         customThread.start();

         final CustomRunnable customRunnable = new CustomRunnable();
         final Thread runnableThread = new Thread(customRunnable);
         runnableThread.start();

         final CustomCallable customCallable = new CustomCallable();
         final Thread thread = new Thread((Runnable) customCallable);
         thread.start();
    }

}
