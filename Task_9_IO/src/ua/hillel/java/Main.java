package ua.hillel.java;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("1. File creation");
        FileCreation fileCreation = new FileCreation();
        String text = "To the town of Agua Fria rode a stranger one fine day " +
                "\nHardly spoke to folks around him, didn't have too much to say " +
                "\nNo one dared to ask his business, no one dared to make a slip " +
                "\nFor the stranger there among them had a big iron on his hip \nBig iron on his hip";
        String filepath = "src/ua/hillel/resources/filefortask1.txt";
        fileCreation.writeWithCreatingFolder(text, filepath);
        String contents = fileCreation.readFile(filepath);
        System.out.println("Contents of a file: \n" + contents);

        System.out.println("2. Registration");
        String filepath2 = "src/ua/hillel/resources/users.txt";
        fileCreation.registration();
        System.out.println("Contents of a file users:\n" + fileCreation.readFile(filepath2));

        System.out.println("3. Login In");
        fileCreation.login();

    }

}
