package ua.hillel.java;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileCreation {

    public void writeWithCreatingFolder(final String text, String path) throws IOException {
        createFolderIfNotExist(path);
        try (final FileWriter fileWriter = new FileWriter(path)) {
            fileWriter.write(text);
            System.out.println("File is created.");
        } catch(IOException e) {
            System.out.println("Something went wrong is file creation");
            e.printStackTrace();
        }
    }

    public void createFolderIfNotExist(String path) throws IOException {
        final Path pathToFile = Paths.get(path);
        if (!Files.exists(pathToFile.getParent())) {
            Files.createDirectories(pathToFile.getParent());
        }
    }

    public String readFile(String path){
        Scanner scanner = null;
        try {
            scanner = new Scanner(Paths.get(path), StandardCharsets.UTF_8.name());
            String data = scanner.useDelimiter("\\A").next();
            return data;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }finally {
            if (scanner !=null)
                scanner.close();
        }
    }

    public void registration() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a login: ");
        String email = scanner.nextLine();
        System.out.println("Input a pass: ");
        String pass = scanner.nextLine();
        System.out.println("Input a pass again: ");
        String pass2 = scanner.nextLine();
        if(pass.equals(pass2)){
            String path = "src/ua/hillel/resources/users.txt";
            writeWithCreatingFolder(email+","+pass, path);
        } else {
            System.out.println("Passwords are different.");
            System.exit(0);
        }
    }

    public void login() throws IOException{
        Scanner scanner = new Scanner(System.in);
        String pass;
        String login;
        try (final FileReader fileReader = new FileReader("src/ua/hillel/resources/users.txt");
             final Scanner scannerforreading = new Scanner(fileReader)) {
            while (scannerforreading.hasNextLine()) {
                String[] logindata = scannerforreading.nextLine().split(",");
                System.out.println("Int login: ");
                login = scanner.nextLine();
                if(logindata[0].equals(login)){
                    for (int i = 0; i < 3; i++){
                        System.out.println("Int pass: ");
                        pass = scanner.nextLine();
                        if(logindata[1].equals(pass)){
                            System.out.println("Good morning!!");
                            System.exit(0);
                        }else {
                            System.out.println("Wrong pass try again, you got: " + (3 - (i+1)) + " tries left.");
                        }
                    }
                    System.out.println("Try again later");
                    System.exit(0);
                }
            }
        }

    }
}
