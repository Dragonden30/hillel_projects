package ua.hillel.java.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString

public class Accounts {
    private int id;
    private int clientId;
    private BigDecimal number;
    private String value;
}
