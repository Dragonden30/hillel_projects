package ua.hillel.java.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;

@Getter
@Setter
@ToString

public class Client {
    private int id;
    private String name;
    private String email;
    private Long phone;
    private String about;
}
