package ua.hillel.java.service;

import ua.hillel.java.Database;
import ua.hillel.java.entity.Statuses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatusesService {
    Connection connection = Database.getConnection();

    public void create(Statuses status) {
        String sql = "INSERT INTO statuses (alias, description) VALUES (?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, status.getAlias());
            statement.setString(2, status.getDescription());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Statuses read(int id) {
        String sql = "SELECT * FROM statuses WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Statuses statuses = new Statuses();
                    statuses.setId(resultSet.getInt("id"));
                    statuses.setAlias(resultSet.getString("alias"));
                    statuses.setDescription(resultSet.getString("description"));
                    return statuses;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void update(Statuses status) {
        String sql = "UPDATE statuses SET alias = ?, description = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, status.getAlias());
            statement.setString(2, status.getDescription());
            statement.setInt(3, status.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        String sql = "DELETE FROM statuses WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
