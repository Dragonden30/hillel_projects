package ua.hillel.java.service;

import ua.hillel.java.Database;
import ua.hillel.java.entity.Accounts;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountsService {
    Connection connection = Database.getConnection();

    public void create(Accounts account) {
        String sql = "INSERT INTO accounts (client_id, number, value) VALUES (?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, account.getClientId());
            statement.setBigDecimal (2, account.getNumber());
            statement.setString(3, account.getValue());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Accounts read(int id) {
        String sql = "SELECT * FROM accounts WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Accounts account = new Accounts();
                    account.setId(resultSet.getInt("id"));
                    account.setClientId(resultSet.getInt("client_id"));
                    account.setNumber(resultSet.getBigDecimal ("number"));
                    account.setValue(resultSet.getString("value"));
                    return account;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void update(Accounts account) {
        String sql = "UPDATE accounts SET client_id = ?, number = ?, value = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, account.getClientId());
            statement.setBigDecimal (2, account.getNumber());
            statement.setString(3, account.getValue());
            statement.setInt(4, account.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        String sql = "DELETE FROM accounts WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<String> getAccountNumbersByValue(BigDecimal minValue) {
        List<String> accountNumbers = new ArrayList<>();
        String sql = "SELECT number FROM accounts WHERE value > ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBigDecimal (1, minValue);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String accountNumber = resultSet.getString("number");
                    accountNumbers.add(accountNumber);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accountNumbers;
    }
}
