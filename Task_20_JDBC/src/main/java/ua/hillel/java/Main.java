package ua.hillel.java;

import ua.hillel.java.entity.Accounts;
import ua.hillel.java.entity.Client;
import ua.hillel.java.entity.Statuses;
import ua.hillel.java.service.AccountsService;
import ua.hillel.java.service.ClientService;
import ua.hillel.java.service.StatusesService;

//import java.sql.Connection;
import java.math.BigDecimal;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //final Connection connection = Database.getConnection();

        final ClientService clientService = new ClientService();
        final List<Client> clients = clientService.getAll();
        final Client read = clientService.read(1);

        System.out.println("AllRead Client");
        clients.forEach(System.out::println);
        System.out.println("Read Client");
        System.out.println(read);
        System.out.println("Clients by Account Client");
        System.out.println(clientService.getClientsByAccountId(1));
        
        final StatusesService statusesService = new StatusesService();
        final Statuses readStatus = statusesService.read(1);

        System.out.println("Read Status");
        System.out.println(readStatus);

        final AccountsService accountsService = new AccountsService();
        final Accounts readAccounts = accountsService.read(1);

        System.out.println("Read Accounts");
        System.out.println(readAccounts);
        System.out.println("Number by value");
        System.out.println(accountsService.getAccountNumbersByValue(BigDecimal.valueOf(1344)));


    }
}
