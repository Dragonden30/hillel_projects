package ua.hillel.java;

public class Finals {

    //finals

    // Yoy cant create setter because you cant modify it, also you cant create constructor without starting
    // parameters values

    private final int intfinal;
    private final double doublefinal;
    private final String stringfinal;

    //public Finals() {
    //}

    public Finals(int intfinal, double doyblefinal, String stringfinal) {
        this.intfinal = intfinal;
        this.doublefinal = doyblefinal;
        this.stringfinal = stringfinal;
    }

    public int getIntfinal() {
        return intfinal;
    }

    public double getDoublefinal() {
        return doublefinal;
    }

    public String getStringfinal() {
        return stringfinal;
    }
}
