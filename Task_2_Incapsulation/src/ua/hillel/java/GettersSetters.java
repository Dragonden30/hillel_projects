package ua.hillel.java;

public class GettersSetters {

    private int intvar;
    private short shortvar;
    private long longvar;
    private float floatvar;
    private char charvar;
    private boolean boolvar;
    private byte bytevar;
    private double doublevar;
    private String someString;

    public GettersSetters() {
    }

    public GettersSetters(int intvar) {
        this.intvar = intvar;
    }

    public GettersSetters(int intvar, short shortvar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar, float floatvar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
        this.floatvar = floatvar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar, float floatvar, char charvar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
        this.floatvar = floatvar;
        this.charvar = charvar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar, float floatvar, char charvar, boolean boolvar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
        this.floatvar = floatvar;
        this.charvar = charvar;
        this.boolvar = boolvar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar, float floatvar, char charvar, boolean boolvar,
                          byte bytevar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
        this.floatvar = floatvar;
        this.charvar = charvar;
        this.boolvar = boolvar;
        this.bytevar = bytevar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar, float floatvar, char charvar, boolean boolvar,
                          byte bytevar, double doublevar) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
        this.floatvar = floatvar;
        this.charvar = charvar;
        this.boolvar = boolvar;
        this.bytevar = bytevar;
        this.doublevar = doublevar;
    }

    public GettersSetters(int intvar, short shortvar, long longvar, float floatvar, char charvar, boolean boolvar,
                          byte bytevar, double doublevar, String someString) {
        this.intvar = intvar;
        this.shortvar = shortvar;
        this.longvar = longvar;
        this.floatvar = floatvar;
        this.charvar = charvar;
        this.boolvar = boolvar;
        this.bytevar = bytevar;
        this.doublevar = doublevar;
        this.someString = someString;
    }

    public int getIntvar() {
        return intvar;
    }

    public void setIntvar(int intvar) {
        this.intvar = intvar;
    }

    public short getShortvar() {
        return shortvar;
    }

    public void setShortvar(short shortvar) {
        this.shortvar = shortvar;
    }

    public long getLongvar() {
        return longvar;
    }

    public void setLongvar(long longvar) {
        this.longvar = longvar;
    }

    public float getFloatvar() {
        return floatvar;
    }

    public void setFloatvar(float floatvar) {
        this.floatvar = floatvar;
    }

    public char getCharvar() {
        return charvar;
    }

    public void setCharvar(char charvar) {
        this.charvar = charvar;
    }

    public boolean getBoolvar() {
        return boolvar;
    }

    public void setBoolvar(boolean boolvar) {
        this.boolvar = boolvar;
    }

    public byte getBytevar() {
        return bytevar;
    }

    public void setBytevar(byte bytevar) {
        this.bytevar = bytevar;
    }

    public double getDoublevar() {
        return doublevar;
    }

    public void setDoublevar(double doublevar) {
        this.doublevar = doublevar;
    }

    public String getSomeString() {
        return someString;
    }

    public void setSomeString(String someString) {
        this.someString = someString;
    }
}
