package ua.hillel.java;

public class Main {

    public static void main(String[] args) {

        System.out.println("Task 1 (Getters Setters) \n");
        GettersSetters gettersSetters = new GettersSetters(123, (short) 23, 1234L, 23.5F,
                (char) 95, true, (byte) 12, 23.54, "Getter Setter");
        System.out.println("Int = " + gettersSetters.getIntvar() + "\nShort = " + gettersSetters.getShortvar() +
                "\nLong = " + gettersSetters.getLongvar() + "\nFloat = " + gettersSetters.getFloatvar() +
                "\nChar = " + gettersSetters.getCharvar() + "\nBoolean = " + gettersSetters.getBoolvar() +
                "\nByte = " + gettersSetters.getBytevar() + "\nDouble = " + gettersSetters.getDoublevar() +
                "\nString = " + gettersSetters.getSomeString() + "\n");

        System.out.println("Task 2 (Finals) \n");
        Finals finals = new Finals(2022, 08.09, "Finals cant be modified");
        System.out.println("Final Int= " + finals.getIntfinal() + "\nFinal Double= " + finals.getDoublefinal() +
                "\nFinal String= " + finals.getStringfinal() + "\n");


    }
}
