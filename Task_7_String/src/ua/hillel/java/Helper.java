package ua.hillel.java;

public class Helper {

    public void phoneHelper(String phoneNumber) {
        String pattern = "^(\\+38( )?)((\\(0\\d{2}\\))|0\\d{2})\\d{3}[- .]?\\d{4}$";
        if(phoneNumber.matches(pattern)){
            System.out.printf("This is a valid number: %s.\n", phoneNumber);
        } else {
            System.out.printf("This is not a valid number.\n");
        }
    }

    public void emailHelper(String email) {
        String pattern = "^(.+)@(\\S+)$";
        if(email.matches(pattern)){
            System.out.printf("This is a valid email: %s.\n", email);
        } else {
            System.out.printf("This is not a valid email.\n");
        }
    }

    public void birthdayHelper(String bday) {
        String pattern = "\\d{2}-\\d{2}-\\d{4}$";
        if(bday.matches(pattern)){
            System.out.printf("This is a valid birth day: %s.\n", bday);
        } else {
            System.out.printf("This is not a valid birth day.\n");
        }
    }

}
