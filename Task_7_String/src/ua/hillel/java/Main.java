package ua.hillel.java;

public class Main {

    public static void main(String[] args) {
        System.out.println("Task 1 \n");
        char array[] = {'s','e','p','t','e','m','b','e','r'};
        StringHelper stringHelper = new StringHelper("October", array);

        System.out.println("Task 2 \n");
        Helper helper = new Helper();
        helper.phoneHelper("+38 (096)5691111");
        helper.emailHelper("denking30@gmail.com");
        helper.birthdayHelper("02-07-1999");
    }

}
