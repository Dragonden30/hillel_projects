package ua.hillel.java;

import java.util.Arrays;

public class StringHelper {

    public StringHelper(String SomeString, char[] someChar){

        String charString;
        String concatStrings;

        if(someChar == null){
            System.out.println("Some char is null");
            return;
        } else {
            charString = Arrays.toString(someChar);
            if(SomeString.trim().isEmpty() | charString.trim().isEmpty()){
                System.out.println("Some string is empty");
                return;
            } else {
                SomeString = SomeString.toUpperCase().strip();
                charString = charString.toUpperCase().strip();
                concatStrings = SomeString.concat(" ").concat(charString);
                int numberofChars = concatStrings.length();
                if(numberofChars % 2 == 0) {
                    System.out.println("The length is even. The first char is " +
                            charString.charAt((numberofChars / 2) - 1) + "\nThe second is "
                            + charString.charAt(numberofChars / 2));
                } else {
                    System.out.println("The length is odd. The char is " +
                            charString.charAt(Math.round(numberofChars / 2) - 1));
                }
            }
        }
    }

}
